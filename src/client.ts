import { Client } from 'discord.js';

import settings from './utils/settings';

const client = new Client();
client.login(settings.token);

export default client;
