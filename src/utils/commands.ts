import {
  Guild,
  GuildChannel,
  GuildMember,
  Message,
  Role,
  TextChannel,
  User,
} from 'discord.js';

import { ColdbotUser, GuildConfig, Level } from '../db';
import logger from '../utils/logger';
import settings from '../utils/settings';
import { parseDuration } from '../utils/time';

const MAX_EMBED_LENGTH = 1024;
const SNOWFLAKE_RE = /^\d{10,}$/;
const NAME_RE = /^[a-zA-Z]+$/;

interface ArgOptions {
  name: string;
  type: string;
  optional?: boolean;
  resolve?: boolean;
}

export interface ColdbotCommand {
  name: string;
  category: string;
  level: string;
  description: string;
  args?: Array<ArgOptions>;
  handler: Function;
}

export const commands: Map<string, ColdbotCommand> = new Map();
export const commandsByCategory: Map<string, Array<ColdbotCommand>> = new Map();

export function registerCommand(cmd: ColdbotCommand) {
  commands.set(cmd.name, cmd);

  const category = commandsByCategory.get(cmd.category) || [];
  category.push(cmd);
  commandsByCategory.set(cmd.category, category);
}

async function isAdmin(
  guild: Guild,
  author: User,
  config: GuildConfig,
): Promise<boolean> {
  if (config.admins.has(author.id) || settings.god === author.id) return true;

  const member = await guild.members.fetch(author.id);

  if (member.hasPermission('ADMINISTRATOR')) return true;

  for (const role of config.adminRoles) {
    if (member.roles.cache.has(role)) return true;
  }

  return false;
}

const CHANNEL_TYPES = new Set(['user', 'admin', 'log']);

type Mapper = (msg: Message, opts: ArgOptions, arg: string) => any;
type MapperIndex = { [key: string]: Mapper | undefined };

const MENTION_RE = /<[^\d]+(\d+)>/;

const mappers: MapperIndex = {
  adminRole: async (msg: Message, opts: ArgOptions, arg: string) => {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.adminRoles.has(arg)) {
      await msg.channel.send(`${opts.name} must be a valid role.`);
      return;
    }

    return arg;
  },
  channel: async (msg: Message, opts: ArgOptions, arg: string) => {
    const match = MENTION_RE.exec(arg);
    const channel = msg.guild.channels.cache.get(match ? match[1] : arg);

    if (channel && channel.type !== 'text') {
      await msg.channel.send(`${opts.name} must be a valid text channel.`);
      return;
    }

    if (opts.resolve && channel) return channel;
    return match ? match[1] : arg;
  },
  channelOrCategory: async (msg: Message, opts: ArgOptions, arg: string) => {
    const match = MENTION_RE.exec(arg);
    const channel = msg.guild.channels.cache.get(match ? match[1] : arg);

    if (channel && channel.type !== 'text' && channel.type !== 'category') {
      await msg.channel.send(
        `${opts.name} must be a text or category channel.`,
      );
      return;
    }

    return match ? match[1] : arg;
  },
  channelType: async (msg: Message, opts: ArgOptions, arg: string) => {
    if (!CHANNEL_TYPES.has(arg)) {
      await msg.channel.send('That is not a valid channel type.');
      return;
    }

    return arg;
  },
  command: async (msg: Message, opts: ArgOptions, arg: string) => {
    if (!commands.has(arg)) {
      await msg.channel.send(`${opts.name} must be a valid command.`);
      return;
    }

    return opts.resolve ? commands.get(arg) : arg;
  },
  existingLevel: async (msg: Message, opts: ArgOptions, arg: string) => {
    const config = await GuildConfig.fetch(msg.guild.id);
    const i = config.levels.findIndex((l) => l.name === arg);

    if (i === -1) {
      await msg.channel.send(`${opts.name} must be an existing level name.`);
      return;
    }

    return opts.resolve ? config.levels[i] : arg;
  },
  float: async (msg: Message, opts: ArgOptions, arg: string) => {
    const num = parseFloat(arg);

    if (isNaN(num) || !isFinite(num)) {
      await msg.channel.send(`${opts.name} must be a valid float.`);
      return;
    }

    return opts.resolve ? num : arg;
  },
  duration: async (msg: Message, opts: ArgOptions, arg: string) => {
    const num = parseDuration(arg);

    if (!num) {
      await msg.channel.send(
        `${opts.name} must be a duration (example: "3hours").`,
      );
      return;
    }

    return opts.resolve ? num : arg;
  },
  levelName: async (msg: Message, opts: ArgOptions, arg: string) => {
    const config = await GuildConfig.fetch(msg.guild.id);
    const i = config.levels.findIndex((l) => l.name === arg);

    if (i !== -1) {
      await msg.channel.send('That level name is already in use.');
      return;
    }

    if (!/^[0-9a-z-]+$/.test(arg)) {
      await msg.channel.send('Invalid level name.');
      return;
    }

    return arg;
  },
  positiveNumber: async (msg: Message, opts: ArgOptions, arg: string) => {
    const num = parseFloat(arg);

    if (isNaN(num) || !isFinite(num)) {
      await msg.channel.send(`${opts.name} must be a valid float.`);
      return;
    }

    if (num < 0) {
      await msg.channel.send(`${opts.name} must be positive.`);
    }

    return opts.resolve ? num : arg;
  },
  role: async (msg: Message, opts: ArgOptions, arg: string) => {
    const match = MENTION_RE.exec(arg);
    const role = msg.guild.roles.cache.get(match ? match[1] : arg);

    if (!role) {
      await msg.channel.send(`${opts.name} must be a valid role.`);
      return;
    }

    if (opts.resolve) return role;
    return match ? match[1] : arg;
  },
  snowflake: async (msg: Message, opts: ArgOptions, arg: string) => {
    if (!SNOWFLAKE_RE.test(arg)) {
      await msg.channel.send(`${opts.name} must be a valid snowflake.`);
      return;
    }

    return arg;
  },
  user: async (msg: Message, opts: ArgOptions, arg: string) => {
    const match = MENTION_RE.exec(arg);
    const member = await msg.guild.members.fetch(match ? match[1] : arg);

    if (!member) {
      await msg.channel.send(`Couldn't find that user.`);
      return;
    }

    if (opts.resolve) return member;
    return match ? match[1] : arg;
  },
  userAdmin: async (msg: Message, opts: ArgOptions, arg: string) => {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.admins.has(arg)) {
      await msg.channel.send('Could not find an admin with that id.');
      return;
    }

    return arg;
  },
};

async function validateArgs(
  msg: Message,
  command: ColdbotCommand,
  args: Array<string>,
): Promise<Array<any> | null> {
  if (!command.args) return [];

  const result = [];

  for (let i = 0; i < command.args.length; i++) {
    if (i >= args.length) {
      if (!command.args[i].optional) return null;

      result.push(undefined);
      continue;
    }

    const opts = command.args[i];
    const handler = mappers[opts.type];
    if (!handler) throw new Error(`Unknown argument type: ${opts.type}`);

    const arg = await handler(msg, opts, args[i]);

    if (arg === undefined) return null;
    result.push(arg);
  }

  return result;
}

export async function runCommand(
  msg: Message,
  name: string,
  args: Array<string>,
): Promise<void> {
  const config = await GuildConfig.fetch(msg.guild.id);
  const command = commands.get(name);

  let found = false;

  const inAdminChannel =
    !config.adminChannel || msg.channel.id === config.adminChannel;
  const inUserChannel =
    !config.userChannel || msg.channel.id === config.userChannel;
  const isGod = settings.god === msg.author.id;

  if (command) {
    if (command.level === 'user') found = inAdminChannel || inUserChannel;
    if (command.level === 'admin') found = inAdminChannel;
    if (!found) found = isGod;
  }

  // Flow doesn't understand that `found` can only be true if `command` is truthy.
  if (command && found) {
    await msg.react('👀');
    if (
      command.level === 'admin' &&
      !(await isAdmin(msg.guild, msg.author, config))
    ) {
      await msg.channel.send(`You don't have permission to use that command.`);
      return;
    }

    const validatedArgs = await validateArgs(msg, command, args);

    if (validatedArgs) {
      try {
        await command.handler(msg, ...validatedArgs);
      } catch (err) {
        logger.error('Got error running command', err);
        await msg.channel.send(`I'm not feeling very well.`);
      }
    }
  } else {
    if (inAdminChannel || inUserChannel || isGod) {
      await msg.react('👀');
      await msg.channel.send(`I don't recognize that command.`);
    }
  }
}

export async function lookupUser(
  guild: Guild,
  userId: string,
): Promise<[ColdbotUser, string]> {
  const user = await ColdbotUser.fetch(guild.id, userId);
  let member = null;
  try {
    member = await guild.members.fetch(userId);
  } catch (err) {
    logger.debug(`Unable to find user ${userId}, defaulting to snowflake`);
  }

  return [user, member ? member.user.tag : `${userId}-left`];
}

function splitRow(row: string) {
  const result = [];

  while (row.length > 1024) {
    const next = row.slice(1024);
    result.push(`${row.slice(0, 1023)}\n`);
    row = next;
  }

  result.push(row);
  return result;
}

function splitList(list: Array<string>) {
  const result = [];

  for (const row of list) {
    result.push(...splitRow(row));
  }

  return result;
}

interface EmbedField {
  name: string;
  value?: string;
}

export function generateEmbedList(
  list: Array<string>,
  name: string,
  numeric: boolean = false,
): Array<EmbedField> {
  const chunks = [];
  let current = '';
  const sortFn = numeric
    ? (a: string, b: string) => parseInt(a, 10) - parseInt(b, 10)
    : (a: string, b: string) => (a > b ? 1 : b > a ? -1 : 0);
  const sorted = list.slice().sort(sortFn);

  for (const row of splitList(sorted)) {
    if (current.length + row.length > MAX_EMBED_LENGTH) {
      chunks.push(current);
      current = '';
    }

    current += `${row}\n`;
  }

  chunks.push(current);

  const fields = [];
  let i = 1;
  const makeName = () =>
    chunks.length === 1 ? name : `${name} (${i}/${chunks.length})`;
  let field: EmbedField = { name: makeName() };
  for (const chunk of chunks) {
    field.value = chunk;
    fields.push(field);
    i += 1;
    field = { name: makeName() };
  }

  return fields;
}

function printArg(arg: ArgOptions) {
  const nameAndType = `${arg.name}:${arg.type}`;

  return arg.optional ? `[${nameAndType}]` : nameAndType;
}

export function getUsage(command: ColdbotCommand): string {
  if (command.args) {
    const args = command.args.map(printArg).join(' ');

    return `${command.name} ${args}`;
  }

  return command.name;
}

export function displayRole(guild: Guild, roleId: string): string {
  try {
    const role = guild.roles.cache.get(roleId);
    return role ? role.name : roleId;
  } catch (err) {
    return roleId;
  }
}

export async function displayUser(
  guild: Guild,
  userId: string,
): Promise<string> {
  try {
    const member = await guild.members.fetch(userId);
    return member ? member.user.tag : userId;
  } catch (err) {
    return userId;
  }
}

export function displayChannel(guild: Guild, channelId: string): string {
  try {
    const channel = guild.channels.cache.get(channelId);
    return channel ? channel.name : channelId;
  } catch (err) {
    return channelId;
  }
}
