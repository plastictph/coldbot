import * as fs from 'fs';

import logger from './logger';

const CONFIG_PATH = process.env.BOT_CONFIG || 'data/config.json';
const VERSION_PATH = process.env.VERSION_FILE || 'static/version';
const DEFAULTS = {
  prefix: '%',
  god: '152969123712729089',
  flushInterval: 5 * 60 * 1000,
  logSize: 1024 * 1024 * 5,
  statsDuration: 4 * 60 * 60 * 1000,
};

function loadConfig() {
  const config = JSON.parse(fs.readFileSync(CONFIG_PATH).toString('utf-8'));
  const pkg = JSON.parse(fs.readFileSync('package.json').toString('utf-8'));
  try {
    const version = fs.readFileSync(VERSION_PATH).toString('utf-8').trim();
    config.version = `${pkg.version} (SHA: ${version})`;
  } catch (err) {
    config.version = `${pkg.version} (DEVELOPMENT)`;
  }
  return config;
}

const settings = {
  ...DEFAULTS,
  ...loadConfig(),
};

export default settings;
