import { Guild, User } from 'discord.js';

import { ColdbotUser, GuildConfig, Level } from '../db';
import logger from './logger';
import { displayRole } from './commands';

const after = (ms: number, fn: () => Promise<void>) =>
  new Promise((resolve) => {
    setTimeout(async () => {
      await fn();
      resolve();
    });
  });

async function syncRoles(
  author: User,
  guild: Guild,
  config: GuildConfig,
  [toAdd, toRemove]: [Array<string>, Array<string>],
): Promise<void> {
  const member = await guild.members.fetch(author.id);
  const actuallyAdd = toAdd.filter((id) => !member.roles.cache.has(id));
  const actuallyRemove = toRemove.filter((id) => member.roles.cache.has(id));

  if (actuallyAdd.length) {
    const roleNames = actuallyAdd.map((id) => displayRole(guild, id));
    const info = `roles ${roleNames.join(', ')} to ${author.tag} :: ${
      author.id
    }`;

    try {
      await member.roles.add(actuallyAdd);
      await config.log(guild, `Added ${info}.`);
      logger.info(`Added ${info}.`);
    } catch (e) {
      logger.error('Unable to add roles to user', e, {
        userId: author.id,
        tag: author.tag,
        actuallyAdd,
      });
      await config.log(guild, `Failed to add ${info}.`);
    }
  }

  if (actuallyRemove.length) {
    const roleNames = actuallyRemove.map((id) => displayRole(guild, id));
    const info = `roles ${roleNames.join(', ')} from ${author.tag} :: ${
      author.id
    }`;

    // This is a hack, for some reason discord.js (or the API) will not let you
    // add roles then remove roles back to back like this, even with the `await`.
    await after(1000, async () => {
      try {
        await member.roles.remove(actuallyRemove);
        await config.log(guild, `Removed ${info}.`);
        logger.info(`Removed ${info}.`);
      } catch (e) {
        logger.error('Unable to remove roles from user', e, {
          userId: author.id,
          actuallyRemove,
        });
        await config.log(guild, `Failed to ${info}.`);
      }
    });
  }
}

function roleChangesForLevel(
  index: number,
  levels: Array<Level>,
): [Array<string>, Array<string>] {
  const toAdd = index === -1 ? [] : [levels[index].role];
  const toRemove = [];

  for (let i = 0; i < levels.length; i++) {
    if (i === index) continue;
    if (i < index && levels[i].sticky) {
      toAdd.push(levels[i].role);
      continue;
    }
    toRemove.push(levels[i].role);
  }

  return [toAdd, toRemove];
}

const updatesInProgress = new Set();

export async function updateRoles(
  member: User,
  guild: Guild,
  user: ColdbotUser,
): Promise<void> {
  const progressKey = `${guild.id}-${member.id}`;
  if (updatesInProgress.has(progressKey)) return;

  const config = await GuildConfig.fetch(guild.id);

  if (config.exemptRoles.size) {
    const guildMember = await guild.members.fetch(member.id);
    for (const role of config.exemptRoles) {
      if (guildMember.roles.cache.has(role)) return;
    }
  }

  updatesInProgress.add(progressKey);

  try {
    const expectedIndex = config.getLevelForPoints(user.points);

    await syncRoles(
      member,
      guild,
      config,
      roleChangesForLevel(expectedIndex, config.levels),
    );
  } catch (err) {
    throw err;
  } finally {
    updatesInProgress.delete(progressKey);
  }
}
