import { createLogger, format, transports } from 'winston';

import settings from './settings';

const level = process.env.LOG_LEVEL || 'debug';

const logger = createLogger({
  level,
  transports: [
    new transports.File({
      filename: 'data/console.log',
      maxsize: settings.logSize,
      format: format.combine(format.timestamp(), format.json()),
    }),
    new transports.File({
      filename: 'data/error.log',
      maxsize: settings.logSize,
      level: 'error',
      format: format.combine(format.timestamp(), format.json()),
    }),
    new transports.Console({
      format: format.combine(format.simple(), format.colorize()),
    }),
  ],
});

export default logger;
