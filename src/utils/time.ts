const MINUTE_SECS = 60;
const HOUR_SECS = 60 * MINUTE_SECS;
const DAY_SECS = 24 * HOUR_SECS;

const DURATIONS = new Set(['s', 'm', 'h', 'd']);

function durationParts(ms: number) {
  const s = Math.floor(ms / 1000);
  const days = Math.floor(s / DAY_SECS);
  const hours = Math.floor((s % DAY_SECS) / HOUR_SECS);
  const minutes = Math.floor((s % HOUR_SECS) / MINUTE_SECS);
  const seconds = Math.floor(s % MINUTE_SECS);

  return [
    { amt: days, title: 'day(s)' },
    { amt: hours, title: 'hour(s)' },
    { amt: minutes, title: 'minute(s)' },
    { amt: seconds, title: 'second(s)' },
  ];
}

export const formatMillis = (ms: number) =>
  durationParts(ms)
    .filter((x) => x.amt > 0)
    .map((x) => `${x.amt} ${x.title}`)
    .join(', ');

export function parseDuration(duration: string): number {
  // Match each duration.
  const durations = duration.match(/\d+\s*\D+s?/g);
  let result = 0;

  if (!durations) return 0;

  for (const d of durations) {
    // Match a number followed by a word.
    const parts = /^(\d+)\s*(\w)/.exec(d);

    if (!parts || !DURATIONS.has(parts[2])) continue;

    const mult = parseInt(parts[1], 10);

    switch (parts[2]) {
      case 's':
        result += mult * 1000;
        break;
      case 'm':
        result += mult * MINUTE_SECS * 1000;
        break;
      case 'h':
        result += mult * HOUR_SECS * 1000;
        break;
      case 'd':
        result += mult * DAY_SECS * 1000;
        break;
      default:
        continue;
    }
  }

  return result;
}
