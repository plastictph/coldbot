import { Guild, Message, PartialMessage, User } from 'discord.js';

import client from './client';
import { closeDatabase, flush } from './db';
import { handleMessage, handleMessageDelete } from './messages';
import logger from './utils/logger';

import './commands';
import { lookupUser } from './utils/commands';

client.on('ready', () => {
  logger.info(`Logged in as ${client.user.tag}!`);
});

client.on('disconnect', () => {
  logger.info('Disconnected...');
});

client.on('message', async msg => {
  logger.debug('Saw message', { guildId: msg.guild.id, user: msg.author.tag, content: msg.content });

  try {
    await handleMessage(msg);
  } catch (err) {
    logger.error('Got error handling message', err);
  }
});

async function messageDelete(msg: Message | PartialMessage) {
  try {
    await handleMessageDelete(msg);
  } catch (err) {
    logger.error('Got error handling message delete', err);
  }
}

client.on('messageDelete', messageDelete);
client.on('messageDeleteBulk', async messages => {
  for (const [id, msg] of messages) {
    await messageDelete(msg);
  }
});

async function guildBan(guild: Guild, user: User) {
  const [coldbotUser, display] = await lookupUser(guild, user.id);
  coldbotUser.resetPoints();
  coldbotUser.save();
}

client.on('guildBanAdd', guildBan)

client.on('error', err => {
  logger.error('Got error from Discord', err);
});

async function gracefulShutdown() {
  logger.warn('Got signal, gracefully shutting down...');

  // Stop our client first so we don't get events after the flush.
  await client.destroy();
  await flush();
  await closeDatabase();

  process.exit(0);
}

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);

// This is used by nodemon.
process.on('SIGUSR2', gracefulShutdown);
