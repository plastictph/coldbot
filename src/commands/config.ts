import { CategoryChannel, Message, Role, TextChannel, User } from 'discord.js';

import { EMBED_COLOR } from '../constants';
import { GuildConfig, Level } from '../db';
import {
  displayChannel,
  displayRole,
  displayUser,
  generateEmbedList,
  registerCommand,
} from '../utils/commands';

registerCommand({
  name: 'multiplier',
  category: 'config',
  level: 'admin',
  description: 'Sets a point multiplier on a channel or category.',
  args: [
    {
      name: 'channel',
      type: 'channelOrCategory',
    },
    {
      name: 'multiplier',
      type: 'positiveNumber',
      optional: true,
      resolve: true,
    },
  ],
  async handler(
    msg: Message,
    channel: string,
    multiplier?: number,
  ): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);
    const display = displayChannel(msg.guild, channel);

    if (multiplier === undefined) {
      const current = config.channelMultipliers.get(channel);

      await msg.channel.send(
        `The multiplier for "${display}" is: ${(current || 1).toFixed(2)}.`,
      );
    } else if (multiplier === 1) {
      config.channelMultipliers.delete(channel);
      config.save();

      await msg.channel.send(`Channel multiplier for "${display}" removed`);
    } else {
      config.channelMultipliers.set(channel, multiplier);
      config.save();

      await msg.channel.send(
        `Channel multiplier for "${display}" set to ${multiplier.toFixed(2)}`,
      );
    }
  },
});

registerCommand({
  name: 'multipliers',
  category: 'config',
  level: 'admin',
  description: 'Displays all multiplers in the guild.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.channelMultipliers.size) {
      await msg.channel.send('All channels have a default multiplier of 1.');
      return;
    }

    const list = [];

    for (const [channelId, multiplier] of config.channelMultipliers) {
      const channel = msg.guild.channels.cache.get(channelId);

      list.push(
        `${
          (channel && channel.name) || channelId
        } has a multiplier of ${multiplier.toFixed(2)}`,
      );
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: generateEmbedList(list, 'Channel Multipliers'),
      },
    });
  },
});

registerCommand({
  name: 'channel',
  category: 'config',
  level: 'admin',
  description: 'Sets a channel as the log, admin, or user channel.',
  args: [
    {
      name: 'type',
      type: 'channelType',
    },
    {
      name: 'channel',
      type: 'channel',
      resolve: true,
    },
  ],
  async handler(
    msg: Message,
    type: string,
    channel: TextChannel,
  ): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (type === 'user') config.userChannel = channel.id;
    if (type === 'admin') config.adminChannel = channel.id;
    if (type === 'log') config.logChannel = channel.id;

    config.save();

    await msg.channel.send(`Set ${channel.name} as the ${type} channel.`);
  },
});

registerCommand({
  name: 'channels',
  category: 'config',
  level: 'admin',
  description: 'Displays all configured channels in the guild.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: [
          {
            name: 'User Channel',
            value: config.userChannel
              ? displayChannel(msg.guild, config.userChannel)
              : 'unset',
          },
          {
            name: 'Log Channel',
            value: config.logChannel
              ? displayChannel(msg.guild, config.logChannel)
              : 'unset',
          },
          {
            name: 'Admin Channel',
            value: config.adminChannel
              ? displayChannel(msg.guild, config.adminChannel)
              : 'unset',
          },
        ],
      },
    });
  },
});

registerCommand({
  name: 'addlevel',
  category: 'config',
  level: 'admin',
  description: 'Adds a new level.',
  args: [
    {
      name: 'name',
      type: 'levelName',
    },
    {
      name: 'points',
      type: 'positiveNumber',
      resolve: true,
    },
    {
      name: 'role',
      type: 'role',
      resolve: true,
    },
  ],
  async handler(
    msg: Message,
    name: string,
    points: number,
    role: Role,
  ): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    const level = { name, role: role.id, points };
    config.addLevel(level);
    config.save();

    await msg.channel.send(`Added level ${name}.`);
  },
});

registerCommand({
  name: 'sticky',
  category: 'config',
  level: 'admin',
  description: 'Toggle the "stickiness" of a level.',
  args: [
    {
      name: 'name',
      type: 'existingLevel',
      resolve: true,
    },
  ],
  async handler(msg: Message, level: Level): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    level.sticky = !level.sticky;
    config.save();

    await msg.channel.send(
      `Level ${level.name} is ${level.sticky ? 'now' : 'no longer'} sticky.`,
    );
  },
});

registerCommand({
  name: 'removelevel',
  category: 'config',
  level: 'admin',
  description: 'Removes a level.',
  args: [
    {
      name: 'name',
      type: 'existingLevel',
    },
  ],
  async handler(msg: Message, name: string): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.removeLevel(name);
    config.save();

    await msg.channel.send(`Removed level ${name}.`);
  },
});

registerCommand({
  name: 'levels',
  category: 'config',
  level: 'admin',
  description: 'Displays all configured levels for the guild.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.levels.length) {
      await msg.channel.send('There are no configured levels.');
      return;
    }

    const list: Array<string> = [];
    const stickyList: Array<string> = [];

    for (const level of config.levels) {
      const which = level.sticky ? stickyList : list;
      which.push(`${level.points} - ${displayRole(msg.guild, level.role)}`);
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: [
          ...(stickyList.length
            ? generateEmbedList(stickyList, 'Sticky Levels', true /* numeric */)
            : []),
          ...(list.length
            ? generateEmbedList(list, 'Levels', true /* numeric */)
            : []),
        ],
      },
    });
  },
});

registerCommand({
  name: 'exemptions',
  category: 'config',
  level: 'admin',
  description: 'Lists all exempt roles.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.exemptRoles.size) {
      await msg.channel.send('There are no configured exemptions.');
      return;
    }

    const list = [];

    for (const role of config.exemptRoles) {
      const guildRole = msg.guild.roles.cache.get(role);

      if (guildRole) list.push(`${guildRole.name} :: ${role}`);
      else list.push(role);
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: generateEmbedList(list, 'Exempt Roles'),
      },
    });
  },
});

registerCommand({
  name: 'addexemption',
  category: 'config',
  level: 'admin',
  description: 'Adds a role that will be exempt from automatic role granting.',
  args: [
    {
      name: 'roleId',
      type: 'role',
      resolve: true,
    },
  ],
  async handler(msg: Message, role: Role): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.exemptRoles.add(role.id);
    config.save();

    await msg.channel.send(
      `Role ${role.name} is exempt from automatic grants.`,
    );
  },
});

registerCommand({
  name: 'removeexemption',
  category: 'config',
  level: 'admin',
  description: 'Removes a role exemption.',
  args: [
    {
      name: 'roleId',
      type: 'role',
      resolve: true,
    },
  ],
  async handler(msg: Message, role: Role): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.exemptRoles.delete(role.id);
    config.save();

    await msg.channel.send(`Role ${role.name} is no longer exempt.`);
  },
});

registerCommand({
  name: 'addadminrole',
  category: 'config',
  level: 'admin',
  description: 'Adds an admin role.',
  args: [
    {
      name: 'role',
      type: 'role',
      resolve: true,
    },
  ],
  async handler(msg: Message, role: Role): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.adminRoles.add(role.id);
    config.save();

    await msg.channel.send(`Role ${role.name} added as admin.`);
  },
});

registerCommand({
  name: 'removeadminrole',
  category: 'config',
  level: 'admin',
  description: 'Removes an admin role.',
  args: [
    {
      name: 'roleId',
      type: 'adminRole',
    },
  ],
  async handler(msg: Message, roleId: string): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.adminRoles.delete(roleId);
    config.save();

    await msg.channel.send(
      `Role ${displayRole(msg.guild, roleId)} removed as admin.`,
    );
  },
});

registerCommand({
  name: 'addadmin',
  category: 'config',
  level: 'admin',
  description: 'Adds an admin.',
  args: [
    {
      name: 'user',
      type: 'user',
      resolve: true,
    },
  ],
  async handler(msg: Message, user: User): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.admins.add(user.id);
    config.save();

    await msg.channel.send(`User ${user.tag} added as admin.`);
  },
});

registerCommand({
  name: 'removeadmin',
  category: 'config',
  level: 'admin',
  description: 'Removes an admin.',
  args: [
    {
      name: 'user',
      type: 'userAdmin',
    },
  ],
  async handler(msg: Message, userId: string): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    config.admins.delete(userId);
    config.save();

    await msg.channel.send(
      `User ${await displayUser(msg.guild, userId)} added as admin.`,
    );
  },
});

registerCommand({
  name: 'admins',
  category: 'config',
  level: 'admin',
  description: 'Displays all configured admins in the guild.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.admins.size) {
      await msg.channel.send('There are no configured admins.');
      return;
    }

    const list = [];

    for (const userId of config.admins) {
      list.push(`${await displayUser(msg.guild, userId)}`);
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: generateEmbedList(list, 'Admins'),
      },
    });
  },
});

registerCommand({
  name: 'adminroles',
  category: 'config',
  level: 'admin',
  description: 'Displays all configured admin roles in the guild.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (!config.adminRoles.size) {
      await msg.channel.send('There are no configured admin roles.');
      return;
    }

    const list = [];

    for (const roleId of config.adminRoles) {
      list.push(`${await displayRole(msg.guild, roleId)}`);
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: generateEmbedList(list, 'Admin Roles'),
      },
    });
  },
});

registerCommand({
  name: 'decay',
  category: 'config',
  level: 'admin',
  description: 'Sets a decay multiplier for inactivity.',
  args: [
    {
      name: 'decayRate',
      type: 'positiveNumber',
      optional: true,
      resolve: true,
    },
  ],
  async handler(msg: Message, decayRate?: number): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    if (decayRate === undefined) {
      if (config.decayRate) {
        await msg.channel.send(
          `The decay rate is currently ${config.decayRate} points per week.`,
        );
      } else {
        await msg.channel.send('Point decay is currently disabled.');
      }
    } else {
      config.decayRate = decayRate;
      config.save();

      if (!decayRate) await msg.channel.send('Point decay disabled.');
      else await msg.channel.send(`Decay rate set to ${decayRate}.`);
    }
  },
});
