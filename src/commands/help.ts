import { TextChannel, Message } from 'discord.js';

import { EMBED_COLOR } from '../constants';
import {
  commandsByCategory,
  generateEmbedList,
  getUsage,
  registerCommand,
  ColdbotCommand,
} from '../utils/commands';

import logger from '../utils/logger';

async function displayCommandList(channel: TextChannel): Promise<void> {
  const fields = [];

  for (const [name, cat] of commandsByCategory) {
    const list = [];

    for (const cmd of cat) {
      list.push(`${cmd.name} - ${cmd.description}`);
    }

    for (const field of generateEmbedList(list, name)) {
      fields.push(field);
    }
  }

  await channel.send({
    embed: {
      fields,
      color: EMBED_COLOR,
    },
  });
}

registerCommand({
  name: 'help',
  category: 'help',
  level: 'user',
  description: 'Displays help for commands.',
  args: [
    {
      name: 'commandName',
      type: 'command',
      optional: true,
      resolve: true,
    },
  ],
  async handler(msg: Message, command?: ColdbotCommand): Promise<void> {
    if (!command) return await displayCommandList(<TextChannel>msg.channel);

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: [
          {
            name: command.name,
            value: `${command.description}\n\n${getUsage(command)}`,
          },
        ],
      },
    });
  },
});
