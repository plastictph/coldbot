import { Message } from 'discord.js';

import { EMBED_COLOR } from '../constants';
import {
  generateEmbedList,
  lookupUser,
  registerCommand,
} from '../utils/commands';
import { formatMillis } from '../utils/time';
import { GuildConfig, Level } from '../db';

registerCommand({
  name: 'reset',
  category: 'points',
  level: 'admin',
  description: 'Resets the points of a user to a level or to 0.',
  args: [
    {
      name: 'userId',
      type: 'snowflake',
    },
    {
      name: 'level',
      type: 'existingLevel',
      optional: true,
      resolve: true,
    },
  ],
  async handler(msg: Message, userId: string, level?: Level): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    const [user, display] = await lookupUser(msg.guild, userId);
    const desiredPoints = level ? level.points : 0;

    user.addPoints(msg.guild, desiredPoints - user.points);
    user.save();

    await msg.channel.send(
      `Points for ${display} were reset to ${desiredPoints}.`,
    );
  },
});

registerCommand({
  name: 'give',
  category: 'points',
  level: 'admin',
  description: 'Gives a user some points.',
  args: [
    {
      name: 'userId',
      type: 'snowflake',
    },
    {
      name: 'points',
      type: 'float',
      resolve: true,
    },
  ],
  async handler(msg: Message, userId: string, points: number): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    const [user, display] = await lookupUser(msg.guild, userId);

    user.addPoints(msg.guild, points);
    user.save();

    await msg.channel.send(
      `User ${display} was given ${points.toFixed(2)} points.`,
    );
  },
});

registerCommand({
  name: 'suspend',
  category: 'points',
  level: 'admin',
  description: 'Suspends point gains for a user.',
  args: [
    {
      name: 'userId',
      type: 'snowflake',
    },
    {
      name: 'duration',
      type: 'duration',
      resolve: true,
      optional: true,
    },
  ],
  async handler(
    msg: Message,
    userId: string,
    duration?: number,
  ): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    const [user, display] = await lookupUser(msg.guild, userId);

    config.suspendUser(userId, duration);
    config.save();

    await msg.channel.send(`Point gains for ${display} have been suspended.`);
  },
});

registerCommand({
  name: 'unsuspend',
  category: 'points',
  level: 'admin',
  description: 'Resumes point gains for a user.',
  args: [
    {
      name: 'userId',
      type: 'snowflake',
    },
  ],
  async handler(msg: Message, userId: string): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);

    const [user, display] = await lookupUser(msg.guild, userId);

    config.resumeUser(userId);
    config.save();

    await msg.channel.send(`Point gains for ${display} have been resumed.`);
  },
});

registerCommand({
  name: 'suspensions',
  category: 'points',
  level: 'admin',
  description: 'Lists all active point suspensions.',
  async handler(msg: Message): Promise<void> {
    const config = await GuildConfig.fetch(msg.guild.id);
    const list = [];
    const now = Date.now();

    for (const [userId, expiration] of config.suspensions) {
      if (expiration && expiration <= now) continue;
      const [, display] = await lookupUser(msg.guild, userId);

      if (expiration) {
        list.push(
          `${display} has point gains suspended for ${formatMillis(
            expiration - now,
          )}`,
        );
      } else {
        list.push(`${display} has point gains suspended permanently`);
      }
    }

    if (!list.length) {
      await msg.channel.send('There are no active suspensions.');
      return;
    }

    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: generateEmbedList(list, 'Suspensions'),
      },
    });
  },
});
