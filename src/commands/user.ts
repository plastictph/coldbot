import { Message, version as discordVersion } from 'discord.js';

import client from '../client';
import settings from '../utils/settings';
import { EMBED_COLOR } from '../constants';
import { lookupUser, registerCommand } from '../utils/commands';
import { formatMillis } from '../utils/time';

// This keeps track of when the bot started for calculating the uptime.
const start = Date.now();

registerCommand({
  name: 'ping',
  category: 'meta',
  level: 'user',
  description: 'Displays the current latency of ColdBot to the Discord API.',
  async handler(msg: Message): Promise<void> {
    await msg.channel.send(
      `Pong! (current latency: ${client.ws.ping.toFixed(1)} ms)`,
    );
  },
});

registerCommand({
  name: 'source',
  category: 'meta',
  level: 'user',
  description: 'Displays a link to the source code of ColdBot.',
  async handler(msg: Message): Promise<void> {
    await msg.channel.send(
      'My source code can be found here: https://gitlab.com/plastictph/coldbot',
    );
  },
});

registerCommand({
  name: 'status',
  category: 'meta',
  level: 'user',
  description: `Displays ColdBot's uptime, latency, and version.`,
  async handler(msg: Message): Promise<void> {
    await msg.channel.send({
      embed: {
        color: EMBED_COLOR,
        fields: [
          {
            name: 'Uptime',
            value: `${formatMillis(Date.now() - start)} since ${new Date(
              start,
            ).toISOString()}`,
          },
          {
            name: 'Latency',
            value: `${client.ws.ping.toFixed(1)} ms`,
          },
          {
            name: 'Version',
            value: settings.version,
          },
          {
            name: 'Discord.js Version',
            value: discordVersion,
          },
        ],
      },
    });
  },
});

registerCommand({
  name: 'points',
  category: 'points',
  level: 'user',
  description: 'Displays how many points a user has accumulated.',
  args: [
    {
      name: 'userId',
      type: 'snowflake',
      optional: true,
    },
  ],
  async handler(msg: Message, userId?: string): Promise<void> {
    const [user, display] = await lookupUser(
      msg.guild,
      userId || msg.author.id,
    );

    await msg.channel.send(
      `${display} has accumulated ${user.points.toFixed(2)} points`,
    );
  },
});
