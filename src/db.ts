import { Guild, TextChannel } from 'discord.js';
import { Database } from 'sqlite3';

import { lookupUser } from './utils/commands';
import logger from './utils/logger';
import settings from './utils/settings';
import { updateRoles } from './utils/levels';

export interface Level {
  name: string,
  points: number,
  role: string,
  sticky?: boolean,
};

interface MultiplierIndex {
  [channelId: string]: number,
}

interface SuspensionIndex {
  [userId: string]: number | null,
}

interface GuildConfigRecord {
  adminRoles: Array<string>,
  admins: Array<string>
  channelMultipliers: MultiplierIndex,
  adminChannel: string | null,
  logChannel: string | null,
  userChannel: string | null,
  levels: Array<Level>,
  exemptRoles: Array<string>,
  suspensions: SuspensionIndex,
  decayRate: number,
}

interface ChannelPointsIndex {
  [channelId: string]: number,
}

interface ColdbotUserRecord {
  points: number,
  lastSeen: number,
}

const db = new Database('data/db');

// Setup tables in case they don't exist.
db.serialize(() => {
  db.run(`
    CREATE TABLE IF NOT EXISTS guilds (
      guild_id TEXT NOT NULL PRIMARY KEY,
      config BLOB NOT NULL
    )
  `);
  db.run(`
    CREATE TABLE IF NOT EXISTS users (
      user_id TEXT NOT NULL,
      guild_id TEXT NOT NULL,
      config BLOB NOT NULL,
      FOREIGN KEY(guild_id) REFERENCES guilds(guild_id),
      PRIMARY KEY (guild_id, user_id)
    )
  `);
});

// Maintain caches in memory to avoid unnecessary I/O.
const userCache: Map<string, ColdbotUser> = new Map();
const guildConfigCache: Map<string, GuildConfig> = new Map();
const ALL_CACHES = [userCache, guildConfigCache];
const WEEK_MS = 1000 * 60 * 60 * 24 * 7;

function mapToObject(m: Map<string, any>): object {
  const result: any = {};

  for (const [k, v] of m) {
    result[k] = v;
  }

  return result;
}

const mapFromObject = (o?: object): Map<string, any> =>
  new Map(Object.entries(o || {}));

async function blobRead(table: string, idds: Array<string>, ids: Array<string>): Promise<string | null> {
  return new Promise((resolve, reject) => {
    const query = `SELECT config FROM ${table} WHERE ${idds.map(k => `${k} = ?`).join(' AND ')}`;
    db.get(query, ids, (err, res) => {
      if (err) {
        logger.error(`Error in blobRead, query: ${query}`);
        logger.error(err);
        reject(err);
      } else {
        resolve(res ? res.config : null);
      }
    })
  });
}

async function blobWrite(table: string, idds: Array<string>, ids: Array<string>, data: string): Promise<void> {
  return new Promise((resolve, reject) => {
    db.run(`INSERT OR REPLACE INTO ${table}(config,${idds.join(',')}) VALUES (?${',?'.repeat(idds.length)})`, [data, ...ids], (err) => {
      if (err) {
        logger.error(`Error in blobWrite for table: ${table}`, err);
        reject(err);
      } else {
        resolve();
      }
    })
  });
}

export class GuildConfig {
  adminRoles: Set<string>;
  admins: Set<string>;
  logChannel: string | null;
  userChannel: string | null;
  adminChannel: string | null;
  channelMultipliers: Map<string, number>;
  levels: Array<Level>;
  shouldSave: boolean;
  guildId: string;
  exemptRoles: Set<string>;
  suspensions: Map<string, number | null>;
  decayRate: number;

  static async fetch(guildId: string): Promise<GuildConfig> {
    if (guildConfigCache.has(guildId)) return guildConfigCache.get(guildId);

    const result = await blobRead('guilds', ['guild_id'], [guildId]);
    const config = new GuildConfig(guildId, result && JSON.parse(result));

    guildConfigCache.set(guildId, config);

    return config;
  }

  async write(): Promise<void> {
    if (!this.shouldSave) return;

    await blobWrite('guilds', ['guild_id'], [this.guildId], JSON.stringify({
      exemptRoles: Array.from(this.exemptRoles),
      adminRoles: Array.from(this.adminRoles),
      admins: Array.from(this.admins),
      channelMultipliers: mapToObject(this.channelMultipliers),
      adminChannel: this.adminChannel,
      logChannel: this.logChannel,
      userChannel: this.userChannel,
      levels: this.levels,
      suspensions: mapToObject(this.suspensions),
      decayRate: this.decayRate,
    }));

    this.shouldSave = false;
  }

  constructor(guildId: string, record: GuildConfigRecord | null) {
    this.guildId = guildId;
    this.adminRoles = new Set(record ? record.adminRoles : []);
    this.admins = new Set(record ? record.admins : []);
    this.channelMultipliers =
      mapFromObject(record ? record.channelMultipliers : {});
    this.adminChannel = record ? record.adminChannel : null;
    this.logChannel = record ? record.logChannel : null;
    this.userChannel = record ? record.userChannel : null;
    this.levels = (record ? record.levels : [])
      .sort((a, b) => a.points - b.points);
    this.shouldSave = false;
    this.exemptRoles = new Set(record ? record.exemptRoles : []);
    this.suspensions = mapFromObject(record ? record.suspensions : {});
    this.decayRate = (record && record.decayRate) || 0;
  }

  addLevel(level: Level): void {
    const i = this.levels.findIndex(l => l.points >= level.points);

    if (i === -1) {
      this.levels.push(level);
    } else {
      this.levels.splice(i, 0, level);
    }
  }

  removeLevel(name: string): void {
    const i = this.levels.findIndex(l => l.name === name);

    if (i !== -1) {
      this.levels.splice(i, 1);
    }
  }

  getLevelForPoints(points: number): number {
    for (let i = this.levels.length - 1; i >= 0; i--) {
      if (this.levels[i].points <= points) return i;
    }
    return -1;
  }

  suspendUser(userId: string, duration?: number): void {
    if (!duration) this.suspensions.set(userId, null);
    else this.suspensions.set(userId, Date.now() + duration);
  }

  resumeUser(userId: string): void {
    this.suspensions.delete(userId);
  }

  async isUserSuspended(guild: Guild, userId: string): Promise<boolean> {
    if (this.suspensions.has(userId)) {
      const expiration = this.suspensions.get(userId);

      if (!expiration || expiration > Date.now()) return true;

      this.suspensions.delete(userId);
      this.save();

      const [user, display] = await lookupUser(guild, userId);
      await this.log(guild, `Point suspension duration for ${display} is finished.`);
    }

    return false;
  }

  async log(guild: Guild, message: string): Promise<void> {
    if (!this.logChannel) return;

    const channel = <TextChannel | null>guild.channels.cache.get(this.logChannel);
    if (!channel) return;

    await channel.send(message);
  }

  save(): void {
    this.shouldSave = true;
  }
}

const roundyAdd = (a: number, b: number) => +(a + b).toFixed(5);

export class ColdbotUser {
  userId: string;
  guildId: string;
  points: number;
  shouldSave: boolean;
  lastSeen: number;

  static async fetch(guildId: string, userId: string): Promise<ColdbotUser> {
    const key = `${userId}-${guildId}`;

    if (userCache.has(key)) return userCache.get(key);

    const result = await blobRead('users', ['guild_id', 'user_id'], [guildId, userId]);
    const user = new ColdbotUser(userId, guildId, result && JSON.parse(result));

    userCache.set(key, user);

    return user;
  }

  async write(): Promise<void> {
    if (!this.shouldSave) return;

    // Deduct points for inactivity.
    const weeksGone = (Date.now() - this.lastSeen) / WEEK_MS;
    const config = await GuildConfig.fetch(this.guildId);
    if (weeksGone > 2 && config.decayRate) {
      this.points = Math.max(0, roundyAdd(this.points, -1 * config.decayRate * weeksGone));
    }

    await blobWrite('users', ['guild_id', 'user_id'], [this.guildId, this.userId], JSON.stringify({
      points: this.points,
      lastSeen: Date.now(),
    }));

    this.shouldSave = false;
  }

  constructor(
    userId: string,
    guildId: string,
    record: ColdbotUserRecord | null,
  ) {
    this.userId = userId;
    this.guildId = guildId;
    this.points = record ? record.points : 0;
    this.lastSeen = record ? record.lastSeen : Date.now();
    this.shouldSave = false;
  }

  async addPoints(
    guild: Guild,
    amt: number,
  ): Promise<void> {
    this.points = Math.max(0, roundyAdd(this.points, amt));

    try {
      const member = await guild.members.fetch(this.userId);
      await updateRoles(member.user, guild, this);
    } catch (err) {
      logger.warn(`Failed to add points for ${this.userId}`);
    }
  }

  async resetPoints(): Promise<void> {
    this.points = 0;
  }

  save(): void {
    this.shouldSave = true;
  }
}

export async function closeDatabase() {
  db.close();
}

export async function flush() {
  logger.debug('Flushing caches...');

  for (const cache of ALL_CACHES) {
    for (const [id, item] of cache) await item.write();
    cache.clear();
  }
}

// Flush the DB cache periodically.
setInterval(flush, settings.flushInterval);
