import { CategoryChannel, Guild, Message, PartialMessage, TextChannel, User } from 'discord.js';

import { ColdbotUser, GuildConfig, Level } from './db';
import { displayUser, runCommand } from './utils/commands';
import logger from './utils/logger';
import settings from './utils/settings';
import { formatMillis } from './utils/time';

const COMMAND_RE = /(\w+)(?:\s+(.*))?/;

let statsPoints = 0;
let statsMessages = 0;
let statsUsers = new Map();
let lastStats = Date.now();

function lookupParents<T>(channel: TextChannel, fn: (id: string) => T | null): T | null {
  let current: TextChannel | CategoryChannel = channel;

  while (current) {
    const result = fn(current.id);
    if (result !== undefined) return result;
    current = current.parent;
  }

  return null;
}

async function channelMultiplier(guildId: string, channel: TextChannel): Promise<number> {
  const config = await GuildConfig.fetch(guildId);
  const found = lookupParents(channel, id => config.channelMultipliers.get(id));

  return found === null ? 1 : found;
}

// This should probably do some kind of analysis on a message to determine basic quality metrics, such as
// checking if it contains only an emote or a common short response like 'lol'.
const messageValue = (s: string) => s.length > 15 ? 1 : 0.75;

async function pointsForMessage(msg: Message | PartialMessage): Promise<number> {
  return await channelMultiplier(msg.guild.id, <TextChannel>msg.channel) * messageValue(msg.content);
}

async function countMessage(msg: Message | PartialMessage, deletion: boolean = false): Promise<void> {
  const config = await GuildConfig.fetch(msg.guild.id);

  if (await config.isUserSuspended(msg.guild, msg.author.id)) return;

  const user = await ColdbotUser.fetch(msg.guild.id, msg.author.id);

  const points = (deletion ? -1 : 1) * await pointsForMessage(msg);
  user.addPoints(msg.guild, points);
  user.save();

  // Track stats for periodic output.
  statsPoints += points;
  statsMessages += 1;
  const userMessages = statsUsers.get(msg.author.id) || 0;
  statsUsers.set(msg.author.id, userMessages + 1);

  if (Date.now() - lastStats  >= settings.statsDuration) {
    logger.info('Writing stats!');
    lastStats = Date.now();
    let activestUser = null;
    for (const [user, points] of statsUsers) {
      if (!activestUser || statsUsers.get(activestUser) < points) activestUser = user;
    }

    if (activestUser) {
      await config.log(
        msg.guild,
        `In the last ${formatMillis(settings.statsDuration)} there were ${statsMessages} messages and ${statsPoints.toFixed(2)} points awarded! The most active user was ${await displayUser(msg.guild, activestUser)}.`,
      );
    }

    statsPoints = 0;
    statsMessages = 0;
    statsUsers = new Map();
  }
}

export async function handleMessage(msg: Message) {
  if (msg.author.bot) return;

  await countMessage(msg);

  if (msg.content.startsWith(settings.prefix)) {
    const match = COMMAND_RE.exec(msg.content.slice(settings.prefix.length));
    if (!match) {
      logger.warn('Unable to process possible command', { content: msg.content });
      return;
    }

    const command = match[1];
    const args = match[2] ? match[2].trim().split(' ') : [];

    await runCommand(msg, command, args);
  }
}

export async function handleMessageDelete(msg: Message | PartialMessage) {
  if (msg.author.bot) return;

  await countMessage(msg, true);
}
