FROM node:14-alpine AS build

ADD . /src

RUN mkdir /app
RUN cd /src && npm install && npx tsc --outDir /app
RUN mv /src/package.json /app/package.json && mv /src/package-lock.json /app/package-lock.json
RUN mv /src/static /app
RUN cd /app && npm install --production

FROM node:14-alpine

COPY --from=build /app /app
WORKDIR /app

CMD ["node", "index.js"]
