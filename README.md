# ColdBot

A Discord bot to collect message statistics and manage TPH active levels.

## How does it work?

ColdBot watches all visible channels for messages. For each message, points are given to a user. A multiplier on these points can be set per channel or per channel category. These are stored in a sqlite3 database along with logs in `/app/data` within the Docker container.

## Configuration

ColdBot has relatively few configuration options. These are read from `config.json` within `/app/data`.

- prefix - The command prefix ColdBot will use
- god - The ID of the user who will always be treated as an admin
- flushInterval - The interval in milliseconds at which the DB cache will be flushed
- logSize - The maximum size in bytes of log files
- statsDuration - The duration in milliseconds for which stats will be collected before being logged to the log channel

## Development

To run locally for development, create a config file in `data/config.json` with a token, then run `docker-compose up`.

Example:

```json
{
  "god": "...",
  "token": "..."
}
```

## Deployment

ColdBot is deployed via Docker. You simply need to pull `plastictph/coldbot` and then run it. There are provided helper [scripts](scripts) to run, update, and deploy the container.
