#!/bin/bash -e

mkdir -p static
git rev-parse master > static/version
docker build -t plastictph/coldbot:latest .
docker push plastictph/coldbot
